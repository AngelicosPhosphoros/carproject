// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "VehicleWheel.h"
#include "VProjectWheelRear.generated.h"

UCLASS()
class UVProjectWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UVProjectWheelRear();
};



