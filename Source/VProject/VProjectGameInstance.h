// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "VProjectGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class VPROJECT_API UVProjectGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Saving")
        bool bLoadLevel = false;
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Saving")
        FString SaveSlotName;
    
    UFUNCTION(BlueprintCallable, Category = "Saving")
        void SaveGame(const UObject* world);

    UFUNCTION(BlueprintCallable, Category = "Saving")
        void LoadGame(const UObject* world);

    UFUNCTION(BlueprintCallable, Category = "Saving")
        static TArray<FString> GetAllSaveGameSlotNames();
	
	
};
