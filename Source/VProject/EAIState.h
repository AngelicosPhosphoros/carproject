#pragma once
/**
*
*/
UENUM()
enum class EAIState:uint8
{
    VE_MOVING_FORWARD,
    VE_MOVING_BACKWARD,
    VE_MOVING_TURN,
    VE_STOPPING,
    VE_END,
    VE_START
};
