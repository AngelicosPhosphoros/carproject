// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "WheeledVehicle.h"
#include "NamedWheeledVehicle.h"
#include "Engine/TargetPoint.h"
#include "VProject/EFuzzyState.h"
#include "VProject/EAIState.h"
#include "AIWheeledVehicle.generated.h"

/**
 * 
 */
UCLASS()
class VPROJECT_API AAIWheeledVehicle : public ANamedWheeledVehicle
{
	GENERATED_BODY()

		

public:
    AAIWheeledVehicle();
	virtual void BeginPlay() override;
    virtual void Tick(float DeltaTime) override;
    UFUNCTION()
     void OnFinish(AActor* MyOverlappedActor, AActor* OtherActor);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float RepeatTime = 0.03;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite)
		float ReachingDistance = 500;
	UPROPERTY(VisibleAnywhere)
		ATargetPoint* Target;
	UPROPERTY(EditAnyWhere)
		TArray<ATargetPoint*> Route;
    bool finished = false;


    UPROPERTY(VisibleAnyWhere)
        float Velocity;
};
