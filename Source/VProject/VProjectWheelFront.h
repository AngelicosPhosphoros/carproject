// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "VehicleWheel.h"
#include "VProjectWheelFront.generated.h"

UCLASS()
class UVProjectWheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UVProjectWheelFront();
};



