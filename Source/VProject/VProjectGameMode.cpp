// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "VProject.h"
#include "VProjectGameMode.h"
#include "VProjectPawn.h"
#include "AIWheeledVehicle.h"
#include "VProjectHud.h"
#include "Engine/TargetPoint.h"
#include "Engine/World.h"
#include "VProjectSaveGame.h"
#include "VProjectGameInstance.h"
#include "EngineUtils.h"
#include "Engine.h"

AVProjectGameMode::AVProjectGameMode()
{
    DefaultPawnClass = AVProjectPawn::StaticClass();
    HUDClass = AVProjectHud::StaticClass();
}


void AVProjectGameMode::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{

    UE_LOG(LogTemp, Log, TEXT("Change Menu Widget Called"));
    if (CurrentWidget != nullptr) {
        CurrentWidget->RemoveFromViewport();
        CurrentWidget = nullptr;
    }
    if (NewWidgetClass != nullptr)
    {
        CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
        if (CurrentWidget != nullptr)
        {
            CurrentWidget->AddToViewport();
        }
    }
}

TArray<ATargetPoint*>& AVProjectGameMode::GetRoute()
{
    if (Route.Num() == 0) {
        UE_LOG(LogTemp, Warning, TEXT("Empty route"));
    }
    return Route;
}




void AVProjectGameMode::BeginPlay()
{
    Super::BeginPlay();
    UVProjectGameInstance* gameInstance = Cast<UVProjectGameInstance>(GetGameInstance());
    if (nullptr != gameInstance && gameInstance->bLoadLevel)
    {
        gameInstance->LoadGame(GetWorld());
    }
}

void AVProjectGameMode::PauseGame(bool bPause)
{
    UE_LOG(LogTemp, Log, TEXT("GameModePaused"))
        APlayerController * controller = Cast<APlayerController>(GetWorld()->GetFirstPlayerController());
    controller->SetPause(bPause);
    controller->bShowMouseCursor = bPause;
    controller->bEnableClickEvents = bPause;
    controller->bEnableMouseOverEvents = bPause;

    if (bPause)
    {
        ChangeMenuWidget(DefaultPauseWidget);
    }
    else {
        ChangeMenuWidget(nullptr);
    }
}

UObject* AVProjectGameMode::GetWorldBP()
{ return GetWorld(); }