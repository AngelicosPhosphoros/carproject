// Fill out your copyright notice in the Description page of Project Settings.

#include "VProject.h"
#include "AIWheeledVehicle.h"
#include "VProjectWheelFront.h"
#include "VProjectWheelRear.h"
#include "VProjectGameMode.h"
#include "EAIState.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "WheeledVehicleMovementComponent4W.h"
#include "Engine/SkeletalMesh.h"
#include "Engine.h"

AAIWheeledVehicle::AAIWheeledVehicle()
{
	// Car mesh
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> CarMesh(TEXT("/Game/Vehicle/Sedan/Sedan_SkelMesh.Sedan_SkelMesh"));
	GetMesh()->SetSkeletalMesh(CarMesh.Object);

	static ConstructorHelpers::FClassFinder<UObject> AnimBPClass(TEXT("/Game/Vehicle/Sedan/Sedan_AnimBP"));
	GetMesh()->SetAnimInstanceClass(AnimBPClass.Class);

	// Simulation
	UWheeledVehicleMovementComponent4W* Vehicle4W = CastChecked<UWheeledVehicleMovementComponent4W>(GetVehicleMovement());

	check(Vehicle4W->WheelSetups.Num() == 4);

	Vehicle4W->WheelSetups[0].WheelClass = UVProjectWheelFront::StaticClass();
	Vehicle4W->WheelSetups[0].BoneName = FName("Wheel_Front_Left");
	Vehicle4W->WheelSetups[0].AdditionalOffset = FVector(0.f, -12.f, 0.f);

	Vehicle4W->WheelSetups[1].WheelClass = UVProjectWheelFront::StaticClass();
	Vehicle4W->WheelSetups[1].BoneName = FName("Wheel_Front_Right");
	Vehicle4W->WheelSetups[1].AdditionalOffset = FVector(0.f, 12.f, 0.f);

	Vehicle4W->WheelSetups[2].WheelClass = UVProjectWheelRear::StaticClass();
	Vehicle4W->WheelSetups[2].BoneName = FName("Wheel_Rear_Left");
	Vehicle4W->WheelSetups[2].AdditionalOffset = FVector(0.f, -12.f, 0.f);

	Vehicle4W->WheelSetups[3].WheelClass = UVProjectWheelRear::StaticClass();
	Vehicle4W->WheelSetups[3].BoneName = FName("Wheel_Rear_Right");
	Vehicle4W->WheelSetups[3].AdditionalOffset = FVector(0.f, 12.f, 0.f);

}

void AAIWheeledVehicle::BeginPlay()
{
	Super::BeginPlay();
	AVProjectGameMode* gm = Cast<AVProjectGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
   // Route[Route.Num() - 1]->OnActorBeginOverlap.AddDynamic(this, &AAIWheeledVehicle::OnFinish);
}

void AAIWheeledVehicle::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    Velocity = FVector::Dist(GetVelocity(), FVector(0, 0, 0));
}

void AAIWheeledVehicle::OnFinish(AActor* MyOverlappedActor, AActor* OtherActor)
{
    UE_LOG(LogTemp, Log, TEXT("On finish called"))
    AVProjectGameMode* gm = Cast<AVProjectGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
}


