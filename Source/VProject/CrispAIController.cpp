// Fill out your copyright notice in the Description page of Project Settings.

#include "VProject.h"
#include "VProjectGameMode.h"
#include "CrispAIController.h"
#include "AIWheeledVehicle.h"
#include "WheeledVehicleMovementComponent.h"
#include "EAIState.h"
#include "Engine.h"

ACrispAIController::ACrispAIController(const FObjectInitializer & ObjectInitializer):Super(ObjectInitializer)
{

}

void ACrispAIController::UpdateMovement()
{
	AAIWheeledVehicle* pawn = CastChecked<AAIWheeledVehicle>(this->GetPawn());
	FVector currentLocation = pawn->GetActorLocation();
	FVector desiredLocation = Destination->GetActorLocation();
	if (GetCurrentState()==EAIState::VE_STOPPING && bLocationReached(currentLocation, desiredLocation)) {
		GetNextDirection();
	}
	TArray<FVector> others;

	CalcState(pawn, Destination->GetActorLocation(), others);
	float steeringInput = CalculateSteeringInput(pawn, desiredLocation, others);
	float throttleInput = CalculateThrottleInput(currentLocation, desiredLocation, others);
	float brakeInput = CalculateBrakeInput();
	pawn->GetVehicleMovementComponent()->SetSteeringInput(steeringInput);
	pawn->GetVehicleMovementComponent()->SetThrottleInput(throttleInput);
	pawn->GetVehicleMovementComponent()->SetBrakeInput(brakeInput);
}

void ACrispAIController::BeginPlay()
{
	Super::BeginPlay();
	AAIWheeledVehicle* pawn = Cast<AAIWheeledVehicle>(GetPawn());
	if (!pawn) {
		UE_LOG(LogTemp, Warning, TEXT("Pawn is null"))
	}
	Destination = pawn->Route[0];
	RepeatTime = pawn->RepeatTime;
	ReachingDistance = pawn->ReachingDistance;
	pawn->Target = Destination;
	GetWorld()->GetTimerManager().SetTimer(TimerHandle,this, &ACrispAIController::UpdateMovement, RepeatTime, true, 1);
}

void ACrispAIController::SetCurrentState(const EAIState new_state)
{
	if (GetCurrentState() == new_state)
		return;
	if ((GetCurrentState() != EAIState::VE_MOVING_FORWARD && new_state == EAIState::VE_MOVING_FORWARD) ||
		(GetCurrentState() != EAIState::VE_MOVING_BACKWARD && GetCurrentState() != EAIState::VE_MOVING_TURN && (new_state == EAIState::VE_MOVING_BACKWARD || new_state == EAIState::VE_MOVING_TURN))
		) 
	{
		MoveStartLocation = GetPawn()->GetActorLocation();
		MoveStartTime = GetWorld()->GetTimeSeconds();
	}
}



bool ACrispAIController::bLocationReached(const FVector & current, const FVector & target) const
{
	return FVector::DistSquaredXY(current, target)<ReachingDistance*ReachingDistance;
}

void ACrispAIController::CalcState(const APawn * pawn, const FVector & target, const TArray<FVector>& others)
{
	if (GetCurrentState() == EAIState::VE_END)
		return;
	if (!Destination) {
		SetCurrentState(EAIState::VE_END);
		return;
	}
	if (bLocationReached(pawn->GetActorLocation(), target) ){
		SetCurrentState(EAIState::VE_STOPPING);
		return;
	}
	FVector current = pawn->GetActorLocation();
    float angle = CalculateTargetAngle(pawn, target);
	// if target in front 
	if ((FMath::Abs(angle)<50)||
		(GetCurrentState() != EAIState::VE_MOVING_TURN && FMath::Abs(angle)<=90))
    {
		SetCurrentState(EAIState::VE_MOVING_FORWARD);
	}
	else {
		// Can reverse
		if (FVector::DistSquared(target, pawn->GetActorLocation()) < 1000) {
			SetCurrentState(EAIState::VE_MOVING_BACKWARD);
		}
		else {
			SetCurrentState(EAIState::VE_MOVING_TURN);
		}
	}
}

void ACrispAIController::GetNextDirection()
{
	if (GetCurrentState() == EAIState::VE_END || !Destination)
		return;
	const TArray<ATargetPoint*>& route = Cast<AAIWheeledVehicle>(GetPawn())->Route;
	int index = route.IndexOfByKey(Destination); // Current destination
	index++; // Next item
	if (index >= route.Num())
	{
		//Destination = NULL; // We reached end
		SetCurrentState(EAIState::VE_END);
		return;
	}
	else
	{
		Destination = route[index];
	}
	if(Destination)
		Cast<AAIWheeledVehicle>(GetPawn())->Target = Destination;
}
float ACrispAIController::CalculateThrottleInput(const FVector & current, const FVector & target, const TArray<FVector>& others) const
{
	if (GetCurrentState() != EAIState::VE_MOVING_FORWARD && GetCurrentState() != EAIState::VE_MOVING_BACKWARD && GetCurrentState() != EAIState::VE_MOVING_TURN)
	{
		return 0;
	}
	float currentDistance = FVector::DistSquared(current, target);
	float initialDistance = FVector::DistSquared(MoveStartLocation, target);
	float notDoneRate = (initialDistance) ? (currentDistance / initialDistance) : 0;
	float multiplier;
	if (notDoneRate < 0.5)
	{
		multiplier = FMath::Pow(0.0000025f * initialDistance + 1, GetWorld()->GetTimeSeconds() - MoveStartTime);
	}
	else 
	{
		multiplier = 1;
	}
	float throttle = notDoneRate * multiplier;
	if (GetCurrentState() == EAIState::VE_MOVING_FORWARD)
	{
		return throttle;
	}
	if (GetCurrentState() == EAIState::VE_MOVING_BACKWARD || GetCurrentState() == EAIState::VE_MOVING_TURN)
		return -throttle;
	return 0;
}

float ACrispAIController::CalculateSteeringInput(const APawn* pawn, const FVector & needed, const TArray<FVector>& others)const
{
	float angle;
	switch (GetCurrentState()) {
	case EAIState::VE_END:return 0;
	case EAIState::VE_MOVING_FORWARD:
		angle = CalculateTargetAngle(pawn, needed);
		return angle/90;
	case EAIState::VE_MOVING_BACKWARD:
		return 0;
	case EAIState::VE_MOVING_TURN:
		return (CalculateTargetAngle(pawn,needed)<0)?+1:-1;
	}
	return 0;
}

float ACrispAIController::CalculateBrakeInput() const
{
	switch (GetCurrentState()) {
	case EAIState::VE_STOPPING: return 1;
	case EAIState::VE_END: return 1;
	default: return 0;
	}
}

float ACrispAIController::CalculateTargetAngle(const APawn * pawn, const FVector & target) const
{
	FVector current = pawn->GetActorLocation();
	return FRotator::NormalizeAxis(((target - current).ToOrientationRotator() - pawn->GetActorRotation()).Yaw);
}
