// Fill out your copyright notice in the Description page of Project Settings.

#include "VProject.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerTargeting.h"
#include "MissionInfo.h"
#include "VProjectPawn.h"
#include "AIWheeledVehicle.h"
#include "PlatformFeatures.h"
#include "GameFramework/SaveGame.h"
#include "Public/SaveGameSystem.h"
#include "VProjectSaveGame.h"
#include "VProjectGameInstance.h"
void UVProjectGameInstance::SaveGame(const UObject * world)
{

    UE_LOG(LogTemp, Log, TEXT("Saving game started"));
    uint32 UserIndex = 0;
    FString saveSlotName = UGameplayStatics::GetCurrentLevelName(world) + TEXT("_");
    int index = 0;
    while (UGameplayStatics::DoesSaveGameExist(saveSlotName + FString::FromInt(index),UserIndex))
    {
        index++;
    }
    saveSlotName += FString::FromInt(index);
    UVProjectSaveGame * SaveGameInstance = Cast<UVProjectSaveGame>(UGameplayStatics::CreateSaveGameObject(UVProjectSaveGame::StaticClass()));

    SaveGameInstance->SaveLevelInfo(world);

    TArray<AActor*> mi;
    UGameplayStatics::GetAllActorsOfClass(world, AMissionInfo::StaticClass(), mi);
    SaveGameInstance->SaveMissionInfo(Cast<AMissionInfo>(mi[0]));

    UGameplayStatics::GetAllActorsOfClass(world, APlayerTargeting::StaticClass(), mi);
    SaveGameInstance->SavePlayerTargeting(Cast<APlayerTargeting>(mi[0]));

    UGameplayStatics::GetAllActorsOfClass(world, AVProjectPawn::StaticClass(), mi);
    SaveGameInstance->SavePlayerInfo(Cast<AVProjectPawn>(mi[0]));

    UGameplayStatics::GetAllActorsOfClass(world, AAIWheeledVehicle::StaticClass(), mi);
    TArray<APawn*> ai_pawns;
    for (AActor* pi : mi) {
        APawn* p = Cast<AAIWheeledVehicle>(pi);
        if (p != nullptr)
            ai_pawns.Add(p);
    }
    SaveGameInstance->SaveAIInfo(ai_pawns);

    UGameplayStatics::SaveGameToSlot(SaveGameInstance, saveSlotName, UserIndex);
}

void UVProjectGameInstance::LoadGame(const UObject * world)
{
    UE_LOG(LogTemp, Log, TEXT("Loading game started"));
    bLoadLevel = false;
    uint32 UserIndex = 0;
    UVProjectSaveGame *SaveGameInstance = Cast<UVProjectSaveGame>(UGameplayStatics::LoadGameFromSlot(SaveSlotName, UserIndex));
    if (SaveGameInstance == nullptr)
        return;
    TArray<AActor*> mi;
    UGameplayStatics::GetAllActorsOfClass(world, AMissionInfo::StaticClass(), mi);
    SaveGameInstance->LoadMissionInfo(Cast<AMissionInfo>(mi[0]));

   

    UGameplayStatics::GetAllActorsOfClass(world, APlayerTargeting::StaticClass(), mi);
    SaveGameInstance->LoadPlayerTargeting(Cast<APlayerTargeting>(mi[0]));
    Cast<APlayerTargeting>(mi[0])->Jump();


    UGameplayStatics::GetAllActorsOfClass(world, AVProjectPawn::StaticClass(), mi);
    SaveGameInstance->LoadPlayerInfo(Cast<AVProjectPawn>(mi[0]));

    UGameplayStatics::GetAllActorsOfClass(world, AAIWheeledVehicle::StaticClass(), mi);
    TArray<APawn*> ai_pawns;
    for (AActor* pi : mi) {
        APawn* p = Cast<AAIWheeledVehicle>(pi);
        if (p != nullptr)
            ai_pawns.Add(p);
    }
    SaveGameInstance->LoadAIInfo(ai_pawns);
}

TArray<FString> UVProjectGameInstance::GetAllSaveGameSlotNames()
{
    TArray<FString> ret;

    ISaveGameSystem* SaveSystem = IPlatformFeaturesModule::Get().GetSaveGameSystem();

    // If we have a save system and a valid name..
    if (SaveSystem)
    {
        // From SaveGameSystem.h in the Unreal source code base.
        FString saveGamePath = FString::Printf(TEXT("%s/SaveGames/"), *FPaths::GameSavedDir());
        FPaths::NormalizeDirectoryName(saveGamePath);
        UE_LOG(LogTemp, Log, TEXT("Search path %s"), *saveGamePath);

        IFileManager& FileManager = IFileManager::Get();

        FString FinalPath = saveGamePath + "/" + "*.sav";
        FileManager.FindFiles(ret, *FinalPath, true, false);
    }

    for (auto it = ret.CreateIterator(); it; it++)
    {
        *it = (*it).Left((*it).Len() - 4);
    }

    return ret;
}
