// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "Engine/TargetPoint.h"
#include "VProject/AIWheeledVehicle.h"
#include "Engine/World.h"
#include "VProject/EFuzzyState.h"
#include "MissionInfo.h"
#include "Engine.h"

#include "FuzzyAIController.generated.h"


/**
 * Class for fuzzy state machine AI
 * Can be in multiple states in same time with different degrees in range from 0.0f to 1.0f
 * Uses EFuzzyState enum to keep states
 */
UCLASS()
class VPROJECT_API AFuzzyAIController : public AAIController
{
	GENERATED_BODY()
	
public:
    
    UPROPERTY(EditAnyWhere, BlueprintReadWrite)
        TMap<EFuzzyState, float> CurrentState;
    // Index of destination in Route
    UPROPERTY(EditAnyWhere, BlueprintReadWrite)
        int DestinationIndex;
    // Distance, which is returns 1 in IsTargetReached
    UPROPERTY(EditAnyWhere, BlueprintReadWrite)
        float ReachedDistanceStrict;
    // if distance more it, IsTargetReached returns 0
    UPROPERTY(EditAnyWhere, BlueprintReadWrite)
        float ReachedDistanceStart;
    UPROPERTY(EditAnyWhere, BlueprintReadWrite)
        float ThrottleStopDistanceStart;
    float BigVelocityLevel;
    float SmallVelocityLevel;
    UPROPERTY(EditAnyWhere)
        float RepeatTime;
    UPROPERTY(EditAnyWhere)
        float SteerThrottleInput; // Input of throttle when car is rotating
    UPROPERTY(EditAnyWhere, BlueprintReadWrite)
        TArray<ATargetPoint*> Route;
public:
    AFuzzyAIController(const FObjectInitializer & ObjectInitializer);
    // Handle movement. Called by timer
    void UpdateMovement();
    // Initialize route and timers
    virtual void BeginPlay()override;
    FORCEINLINE void SetStateDegree(const EFuzzyState& state, const float& degree);
private:
    void CalculateStates();
    float CalculateThrottleInput()const;
    float CalculateSteeringInput()const;
    float CalculateBrakeInput()const;
    bool CalculateHandBrakeInput()const;
    // fuzzy logic conditions
    FORCEINLINE float IsTargetReached(const FVector& loc, const FVector& target) const;
    FORCEINLINE float IsTargetReached(const FVector& loc, const FVector& target, const float StartDistance) const;
    FORCEINLINE float IsVelocityBig() const;
    FORCEINLINE float IsAngleBig(float angle)const;
    // Yaw difference between toTargetVector and pawn rotation
    FORCEINLINE float CalculateTargetAngle(const APawn* pawn, const FVector& targetLoc) const;
    FORCEINLINE FVector GetDestination(int tNum) const;
private:
    UPROPERTY(transient)
        FTimerHandle TimerHandle;
    AMissionInfo* MissionInfo;
private: //Fuzzy logic
    FORCEINLINE float AND(const float& a, const float& b) const { return FMath::Min(a, b); }
    FORCEINLINE float OR(const float& a, const float& b) const { return FMath::Max(a, b); }
    FORCEINLINE float NOT(const float& a)const { return 1.0f-a; }
};
