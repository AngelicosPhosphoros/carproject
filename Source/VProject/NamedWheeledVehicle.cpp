// Fill out your copyright notice in the Description page of Project Settings.

#include "VProject.h"
#include "NamedWheeledVehicle.h"

void ANamedWheeledVehicle::Tick(float delta_seconds)
{
    Super::Tick(delta_seconds);
    if (knocked || FMath::Abs(GetActorRotation().Roll) >= 80.f)
    {
        float velocity = GetVelocity().Size();
        if (velocity < 10000.0f / 60 / 60)
        {
            if (knocked)
            {
                if (knocked_time >= 5)
                {
                    
                    FRotator rot = GetActorRotation();
                    SetActorRotation(FRotator(rot.Pitch, rot.Yaw, 0), ETeleportType::TeleportPhysics);
                    UE_LOG(LogTemp, Log, TEXT("Actor rotated back"));
                    knocked = false;
                    knocked_time = 0;
                }
            }
            else {
                knocked = true;
            }
            knocked_time += delta_seconds;
        }
    }
    else {
        knocked = false;
        knocked_time = 0;
    }
}
