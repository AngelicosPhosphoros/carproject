// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameFramework/Actor.h"
#include "AIController.h"
#include "Engine/TargetPoint.h"
#include "EAIState.h"
#include "CrispAIController.generated.h"



/**
 * Class with standart Crisp AI
 */
UCLASS()
class VPROJECT_API ACrispAIController : public AAIController
{
	GENERATED_BODY()
public:
	ACrispAIController(const FObjectInitializer & ObjectInitializer);
	void UpdateMovement();
	virtual void BeginPlay() override;
public:
	
	UPROPERTY(EditAnywhere)
	ATargetPoint* Destination;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RepeatTime = 0.03;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite)
	float ReachingDistance = 500;

private:

	FORCEINLINE EAIState GetCurrentState() const { return CurrentState; }
	FORCEINLINE void SetCurrentState(const EAIState state);

	EAIState CurrentState = EAIState::VE_START;
	UPROPERTY(transient)
	FTimerHandle TimerHandle;

	UPROPERTY()
	FVector MoveStartLocation;// Location, where car started to move
	
	float MoveStartTime;

	bool bLocationReached(const FVector& current, const FVector& target) const;
	void CalcState(const APawn* pawn, const FVector& target, const TArray<FVector>& others);
	void GetNextDirection();
	float CalculateThrottleInput(const FVector& current, const FVector& needed, const TArray<FVector>& others) const;
	float CalculateSteeringInput(const APawn* pawn, const FVector& needed, const TArray<FVector>& others) const;
	float CalculateBrakeInput() const;
    // Yaw angle between pawn rotation and vector from pawn to target
	float CalculateTargetAngle(const APawn* pawn, const FVector& target) const;
};

