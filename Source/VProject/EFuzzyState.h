// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
/**
 * 
 */
UENUM()
enum class EFuzzyState :uint8
{
    VE_START            UMETA(DisplayName = "Start"),
    VE_MOVING           UMETA(DisplayName = "Moving"),
    VE_ROTATING		    UMETA(DisplayName = "Rotating"),
    VE_STOPPING         UMETA(DisplayName = "Stopping"),
    //VE_HANDBRAKING      UMETA(DisplayName = "Handbraking"),
    VE_END              UMETA(DisplayName = "End")
};