// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/StaticMeshActor.h"
#include "Engine/TargetPoint.h"
#include "MissionInfo.h"
#include "PlayerTargeting.generated.h"

/**
 * 
 */
UCLASS()
class VPROJECT_API APlayerTargeting : public AStaticMeshActor
{
	GENERATED_BODY()
	
public:
    APlayerTargeting(const FObjectInitializer & ObjectInitializer);
    virtual void BeginPlay() override;
    UFUNCTION(Category = "Event") 
    void OnBeginOverlap(AActor* MyOverlappedActor, AActor* OtherActor);
    
    UFUNCTION(Category = "Event")
        void Jump();

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        AMissionInfo* MissionInfo;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        APawn* PlayerPawn;

    FORCEINLINE int GetCurrentTargetIndex()const { return CurrentTargetIndex; }
    FORCEINLINE void SetCurrentTargetIndex(const int value) {  CurrentTargetIndex = value; }
    FORCEINLINE bool GetBEnd()const { return bEnd; }
    FORCEINLINE void SetBEnd(const bool value) { bEnd = value; }
protected:
    bool bIsOverlapped = false;
    bool bEnd = false;
    int CurrentTargetIndex;
};
