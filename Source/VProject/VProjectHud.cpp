// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "VProject.h"
#include "VProjectHud.h"
#include "VProjectPawn.h"
#include "WheeledVehicle.h"
#include "RenderResource.h"
#include "Shader.h"
#include "Engine/Canvas.h"
#include "WheeledVehicleMovementComponent.h"
#include "Engine/Font.h"
#include "CanvasItem.h"
#include "Engine.h"

// Needed for VR Headset
#if HMD_MODULE_INCLUDED
#include "IHeadMountedDisplay.h"
#endif // HMD_MODULE_INCLUDED 

#define LOCTEXT_NAMESPACE "VehicleHUD"

AVProjectHud::AVProjectHud()
{
	static ConstructorHelpers::FObjectFinder<UFont> Font(TEXT("/Engine/EngineFonts/RobotoDistanceField"));
	HUDFont = Font.Object;
}

void AVProjectHud::DrawHUD()
{
	Super::DrawHUD();

	// Calculate ratio from 720p
	const float HUDXRatio = Canvas->SizeX / 1280.f;
	const float HUDYRatio = Canvas->SizeY / 720.f;

	bool bWantHUD = true;
#if HMD_MODULE_INCLUDED
	if (GEngine->HMDDevice.IsValid() == true)
	{
		bWantHUD = GEngine->HMDDevice->IsStereoEnabled();
	}
#endif // HMD_MODULE_INCLUDED
	// We dont want the onscreen hud when using a HMD device	
	if (bWantHUD == true)
	{
		// Get our vehicle so we can check if we are in car. If we are we don't want onscreen HUD
		AVProjectPawn* Vehicle = Cast<AVProjectPawn>(GetOwningPawn());
		if ((Vehicle != nullptr) && (Vehicle->bInCarCameraActive == false))
		{
			FVector2D ScaleVec(HUDYRatio * 1.4f, HUDYRatio * 1.4f);

			// Speed
			FCanvasTextItem SpeedTextItem(FVector2D(HUDXRatio * 805.f, HUDYRatio * 455), Vehicle->SpeedDisplayString, HUDFont, FLinearColor::White);
			SpeedTextItem.Scale = ScaleVec;
			Canvas->DrawItem(SpeedTextItem);

			// Gear
			FCanvasTextItem GearTextItem(FVector2D(HUDXRatio * 805.f, HUDYRatio * 500.f), Vehicle->GearDisplayString, HUDFont, Vehicle->bInReverseGear == false ? Vehicle->GearDisplayColor : Vehicle->GearDisplayReverseColor);
			GearTextItem.Scale = ScaleVec;
			Canvas->DrawItem(GearTextItem);

            // Winners table
            if (finished)
            {
                const float startX = 100.f, startY = 50.f;
                for (int i = 0; i < WinnersMessage.Num();++i)
                {
                    FCanvasTextItem WinnerTextItem(FVector2D(HUDXRatio * startX, HUDYRatio * (startY + i * 50)), WinnersMessage[i], HUDFont, FLinearColor::White);
                    GearTextItem.Scale = ScaleVec;
                    Canvas->DrawItem(WinnerTextItem);
                }
            }
		}
	}
}

void AVProjectHud::ShowWinners(const TArray<ANamedWheeledVehicle*>& winners, const TArray<float>& times)
{
    if (!(winners.Num() || times.Num()))
        return;
    TArray<FText> n;
    n.Reset(winners.Num()+1);
    n.Emplace(FText::FromString(TEXT("Winners are")));
    for (int i = 0; i < winners.Num();++i)
    {
        FString s;
        s += FString::FromInt(i + 1) + TEXT(":  ");
        s += winners[i]->PawnName.ToString();
        s += TEXT("      Time: ");
        s += FString::SanitizeFloat(times[i]);
        n.Emplace(FText::FromString(s));
    }
    WinnersMessage = n;
    finished = true;
}


#undef LOCTEXT_NAMESPACE
