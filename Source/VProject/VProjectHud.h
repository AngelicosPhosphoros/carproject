// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/HUD.h"
#include "NamedWheeledVehicle.h"
#include "VProjectHud.generated.h"


UCLASS(config = Game)
class AVProjectHud : public AHUD
{
	GENERATED_BODY()

public:
	AVProjectHud();

	/** Font used to render the vehicle info */
	UPROPERTY()
	UFont* HUDFont;

	// Begin AHUD interface
	virtual void DrawHUD() override;
	// End AHUD interface

    bool finished = false;
    UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
    TArray<FText> WinnersMessage;
    void ShowWinners(const TArray<ANamedWheeledVehicle*>& winners, const TArray<float>& times);
};
