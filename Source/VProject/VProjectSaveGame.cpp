// Fill out your copyright notice in the Description page of Project Settings.

#include "VProject.h"
#include "VProjectSaveGame.h"
#include "WheeledVehicleMovementComponent.h"
//#include "Kismet/GameplayStatics.h"
#include "EngineUtils.h"

void UVProjectSaveGame::SaveMissionInfo(AMissionInfo* mission_info)
{
    FinishedActorNames.Reset(mission_info->Finishers.Num());
    FinishedTimes.Reset(mission_info->Finishers.Num());
    for (int i = 0; i < mission_info->Finishers.Num();++i)
    {
        FinishedActorNames.Emplace(mission_info->Finishers[i]->GetFName());
    }
    FinishedTimes = mission_info->FinishTimes;
    bPlayerFinished = mission_info->bPlayerFinished;
}

void UVProjectSaveGame::LoadMissionInfo(AMissionInfo * mission_info)
{
    mission_info->FinishTimes = FinishedTimes;
    mission_info->Finishers.Reset(FinishedActorNames.Num());
    TSet<FName> nameSet(FinishedActorNames);
    TMap<FName, ANamedWheeledVehicle*> foundActors;
    for (TActorIterator<ANamedWheeledVehicle> it(mission_info->GetWorld()); it; ++it) {
        ANamedWheeledVehicle* p = *it;
        if (nameSet.Contains(p->GetFName())) {
            foundActors.Add(p->GetFName(), p);
        }
    }
    for (const FName& name : FinishedActorNames) {
        mission_info->Finishers.Add(foundActors[name]);
    }
    mission_info->bPlayerFinished = bPlayerFinished;
    mission_info->UpdateHUD();
}

void UVProjectSaveGame::SavePlayerTargeting(APlayerTargeting * aPlayerTargeting)
{
    PlayerTargetName = aPlayerTargeting->GetFName();
    PlayerCurrentTargetIndex = aPlayerTargeting->GetCurrentTargetIndex();
    bEnd = aPlayerTargeting->GetBEnd();
}

void UVProjectSaveGame::LoadPlayerTargeting(APlayerTargeting * aPlayerTargeting)
{
    aPlayerTargeting->SetCurrentTargetIndex(PlayerCurrentTargetIndex);
    aPlayerTargeting->SetBEnd(bEnd);
}

void UVProjectSaveGame::SavePlayerInfo(ANamedWheeledVehicle * pawn)
{
    PlayerLocation = pawn->GetActorLocation();
    PlayerRotation = pawn->GetActorRotation();
    PlayerVelocity = pawn->GetVelocity();
}

void UVProjectSaveGame::LoadPlayerInfo(ANamedWheeledVehicle * pawn)
{
    pawn->SetActorLocationAndRotation(PlayerLocation, PlayerRotation, false, nullptr, ETeleportType::TeleportPhysics);
    pawn->GetRootComponent()->ComponentVelocity = PlayerVelocity;
}

void UVProjectSaveGame::SaveAIInfo(const TArray<APawn*>& aiPawns)
{
    AIPawnFNames.Reset(aiPawns.Num());
    AIPawnLocations.Reset(aiPawns.Num());
    AIPawnRotations.Reset(aiPawns.Num());
    AIPawnVelocities.Reset(aiPawns.Num());
    AIPawnStates.Reset();
    for (APawn* pawn : aiPawns)
    {
        AIPawnFNames.Add(pawn->GetFName());
        AIPawnLocations.Add(pawn->GetActorLocation());
        AIPawnRotations.Add(pawn->GetActorRotation());
        AIPawnVelocities.Add(pawn->GetVelocity());
        AFuzzyAIController* controller = Cast<AFuzzyAIController>(pawn->GetController());
        if (nullptr != controller)
        {
            if (controller->CurrentState.Contains(EFuzzyState::VE_END))
            {
                AIPawnStates.Add(pawn->GetFName(), controller->CurrentState[EFuzzyState::VE_END]==1.f);
            }
            else {
                AIPawnStates.Add(pawn->GetFName(), false);
            }
            AIPawnDestinations.Add(pawn->GetFName(), controller->DestinationIndex);
        }
    }
}

void UVProjectSaveGame::LoadAIInfo(TArray<APawn*>& aiPawns)
{
    TMap<FName, APawn*> pawnMap;
    for (APawn* pawn : aiPawns)
    {
        pawnMap.Add(pawn->GetFName(), pawn);
    }
    for (size_t i = 0; i < AIPawnFNames.Num();++i) 
    {
        FName name = AIPawnFNames[i];
        if (!pawnMap.Contains(name)) {
            continue;
        }
        AWheeledVehicle* p = Cast<AWheeledVehicle>(pawnMap[name]);
        if (p == nullptr) {
            continue;
        }
        p->SetActorLocationAndRotation(AIPawnLocations[i], AIPawnRotations[i],false,nullptr,ETeleportType::TeleportPhysics);
        p->GetVehicleMovementComponent()->AddInputVector(AIPawnVelocities[i]);
        AFuzzyAIController* controller = Cast<AFuzzyAIController>(p->GetController());
        if (nullptr != controller && AIPawnStates.Contains(name))
        {
            for (EFuzzyState s = EFuzzyState::VE_START; s != EFuzzyState::VE_END;s = (EFuzzyState)(1 + (uint8)s))
            {
                controller->CurrentState.Add(s, 0);
            }
            controller->CurrentState.Add(EFuzzyState::VE_END, AIPawnStates[name] ? 1 : 0);
            controller->DestinationIndex = AIPawnDestinations[name];
        }
    }
}

void UVProjectSaveGame::SaveLevelInfo(const UObject * world)
{
    LevelName = Cast<UWorld>(world)->GetMapName();
    LevelName.RemoveFromStart(Cast<UWorld>(world)->StreamingLevelsPrefix);
}
