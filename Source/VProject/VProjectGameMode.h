// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Blueprint/UserWidget.h"
#include "Engine/TargetPoint.h"
#include "AIController.h"
#include "VProjectGameMode.generated.h"

UCLASS()
class VPROJECT_API AVProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AVProjectGameMode();
    virtual void BeginPlay() override;
    UFUNCTION(BlueprintCallable, Category = "Pause")
        void PauseGame(bool bPause);
   
    UFUNCTION(BlueprintCallable, Category = "Game Interface")
        void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);
    UFUNCTION(BlueprintCallable, Category = "Game")
        TArray<ATargetPoint*>& GetRoute();

    UFUNCTION(BlueprintCallable, Category = "Game")
        UObject* GetWorldBP();

protected:
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Game Interface")
        TSubclassOf<UUserWidget> DefaultPauseWidget;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Game Interface")
        UUserWidget * CurrentWidget;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Game")
	TArray<ATargetPoint*> Route;

private:
    float StartTime;
};



