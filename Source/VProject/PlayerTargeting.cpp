// Fill out your copyright notice in the Description page of Project Settings.

#include "VProject.h"
#include "Engine.h"
#include "VProjectPawn.h"
#include "PlayerTargeting.h"

APlayerTargeting::APlayerTargeting(const FObjectInitializer & ObjectInitializer) :Super(ObjectInitializer)
{
    bIsOverlapped = false;
    CurrentTargetIndex = 0;
    OnActorBeginOverlap.AddDynamic(this, &APlayerTargeting::OnBeginOverlap);
}

void APlayerTargeting::BeginPlay() {
    TArray<AActor*> foundActors;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), AMissionInfo::StaticClass(), foundActors);
    MissionInfo = Cast<AMissionInfo>(foundActors[0]);
    Jump();
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), AVProjectPawn::StaticClass(), foundActors);
    PlayerPawn = Cast<APawn>(foundActors[0]);
}

void APlayerTargeting::OnBeginOverlap(AActor* MyOverlappedActor, AActor* OtherActor)
{
    if (OtherActor!=PlayerPawn || bIsOverlapped)
        return;
    bIsOverlapped = true;
    TArray<ATargetPoint*>& route = MissionInfo->Route;
    if (route.Num() - 1 > CurrentTargetIndex)
        // Move to next
    {
        CurrentTargetIndex++;
        Jump();
    }
    else
    {
        if(!bEnd)
            MissionInfo->RegisterFinish(PlayerPawn);
        bEnd = true;
    }
    bIsOverlapped = false;
}

void APlayerTargeting::Jump()
{
    if (MissionInfo == nullptr)
        return;
    SetActorLocationAndRotation(MissionInfo->Route[CurrentTargetIndex]->GetActorLocation(), GetActorRotation(), false, nullptr, ETeleportType::TeleportPhysics);
}
