// Fill out your copyright notice in the Description page of Project Settings.

#include "VProject.h"
#include "WheeledVehicleMovementComponent.h"
#include "EFuzzyState.h"
#include "FuzzyAIController.h"
#include "MissionInfo.h"

FORCEINLINE static bool bIsForward(const float& angle) {
    return -90 <= angle &&  angle<=90;
}

AFuzzyAIController::AFuzzyAIController(const FObjectInitializer & ObjectInitializer):Super(ObjectInitializer)
{
    // Init values
    ReachedDistanceStrict = 500;
    ReachedDistanceStart = 1000;
    ThrottleStopDistanceStart = 3000;
    DestinationIndex = -1;
    RepeatTime = 0.03f;
    SteerThrottleInput = 0.25f;
    BigVelocityLevel = 80 * 10000.0f / 60 / 60;
    SmallVelocityLevel = 60*10000.0f/60/60;
    // Initial state
    for (EFuzzyState e = EFuzzyState::VE_START; e <= EFuzzyState::VE_END;e = (EFuzzyState)(1+(uint8)e)) 
    {
        SetStateDegree(e, 0);
    }
    SetStateDegree(EFuzzyState::VE_START, 1);
}

void AFuzzyAIController::UpdateMovement()
{
    AAIWheeledVehicle* pawn = Cast<AAIWheeledVehicle>(GetPawn());
    CalculateStates();
    float throttle = CalculateThrottleInput();
    float steer = CalculateSteeringInput();
    float brake = CalculateBrakeInput();
    bool handbrake = CalculateHandBrakeInput();
    pawn->GetVehicleMovementComponent()->SetSteeringInput(steer);
    pawn->GetVehicleMovementComponent()->SetThrottleInput(throttle);
    pawn->GetVehicleMovementComponent()->SetBrakeInput(brake);
    pawn->GetVehicleMovementComponent()->SetHandbrakeInput(handbrake);
    // This is to debug in Unreal Editor
    pawn->Target = Route[DestinationIndex];
}

void AFuzzyAIController::BeginPlay()
{
    Super::BeginPlay();
    AAIWheeledVehicle* pawn = Cast<AAIWheeledVehicle>(GetPawn());
    if (!pawn) {
        UE_LOG(LogTemp, Warning, TEXT("Pawn is null"))
    }
    TArray<AActor*> missionInfo;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), AMissionInfo::StaticClass(), missionInfo);
    MissionInfo = Cast<AMissionInfo>(missionInfo[0]);
    Route = MissionInfo->Route;
    if (!Route.Num()) {
        UE_LOG(LogTemp, Warning, TEXT("Route is empty"))
    }
    DestinationIndex = 0;
    GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &AFuzzyAIController::UpdateMovement, RepeatTime, true, 1);
}

void AFuzzyAIController::SetStateDegree(const EFuzzyState & state, const float & degree)
{
    if (state == EFuzzyState::VE_END && degree == 1.0f)
    {
        MissionInfo->RegisterFinish(GetPawn());
    }
    if (!CurrentState.Contains(state))
    {
        CurrentState.Add(state, degree);
        return;
    }
    CurrentState[state] = degree;
}



void AFuzzyAIController::CalculateStates()
{
    // Handle end and start
    if (CurrentState[EFuzzyState::VE_START] == 1.0f)
        SetStateDegree(EFuzzyState::VE_START, 0);
    if (CurrentState[EFuzzyState::VE_END] == 1.0f)
        return;

    AAIWheeledVehicle* pawn = Cast<AAIWheeledVehicle>(GetPawn());
    FVector currentLoc = pawn->GetActorLocation();
    FVector targetLoc = GetDestination(0);

    float angle = CalculateTargetAngle(pawn, targetLoc);
    float angleRad = FMath::DegreesToRadians(angle);
    float distance = FMath::Sqrt(FVector::DistSquaredXY(currentLoc, targetLoc));
    float distanceForward = distance* FMath::Cos(angleRad);
    float distanceRight = distance * FMath::Sin(angleRad);
   /* UE_LOG(LogTemp, Log, TEXT("DistanceForward: %f"), distanceForward);
    UE_LOG(LogTemp, Log, TEXT("distanceRight: %f"), distanceRight);
    UE_LOG(LogTemp, Log, TEXT("distance: %f"), distance);
    UE_LOG(LogTemp, Log, TEXT("Cosine: %f"), FMath::Cos(FMath::DegreesToRadians(angle)));
    UE_LOG(LogTemp, Log, TEXT("Angle: %f"), angle);*/
    float val;

    // Moving Forward
    val = AND((distanceForward / distance + 1) / 2,
        NOT(CurrentState[EFuzzyState::VE_END]));
    if (false && DestinationIndex != Route.Num() - 1 && val>0) 
    {
        UE_LOG(LogTemp, Log, TEXT("Val start: %f"), val);
        FVector secondTarget = GetDestination(1);
        float routeChanging = (targetLoc - currentLoc).ToOrientationRotator().Yaw - (secondTarget - targetLoc).ToOrientationRotator().Yaw;
        val  = AND(val,
            OR(0.5f,NOT(AND(AND(IsAngleBig(routeChanging),IsVelocityBig()),IsTargetReached(currentLoc, targetLoc, ThrottleStopDistanceStart))/2)));
        if (IsAngleBig(routeChanging) > 0.5f && FVector::Dist(currentLoc,targetLoc)<10000 && pawn->GetVelocity().Size()>= 2000) {
            val = 0.5;
        }
        //val = (val * 2 + IsTargetReached(targetLoc,currentLoc,ThrottleStopDistanceStart)*(distFwdSecond / distanceSecond + 1) / 2) / (2+ IsTargetReached(targetLoc, currentLoc, ThrottleStopDistanceStart));
        UE_LOG(LogTemp, Log, TEXT("Val end: %f"), val);
    }
    SetStateDegree(EFuzzyState::VE_MOVING, val);

    // Rotating
    val = AND(FMath::Abs(distanceRight / distance),
        NOT(CurrentState[EFuzzyState::VE_END]));
    if (!bIsForward(angle) && distance > ReachedDistanceStart)
        val = OR(NOT(IsTargetReached(currentLoc, targetLoc)),val);
    SetStateDegree(EFuzzyState::VE_ROTATING, val);

    // Stopping
    val = OR(IsTargetReached(currentLoc, targetLoc), CurrentState[EFuzzyState::VE_END]);
    SetStateDegree(EFuzzyState::VE_STOPPING, val);

    // Change target and set end state
    if (IsTargetReached(currentLoc, targetLoc) == 1.0f)
    {
        if (DestinationIndex+1 == Route.Num())
        {
            SetStateDegree(EFuzzyState::VE_END, 1);
        }
        else
        {
            DestinationIndex++;
        }
    }
}

float AFuzzyAIController::CalculateThrottleInput() const
{
    if (CurrentState[EFuzzyState::VE_END] == 1.0f)
        return 0;
    // We return max of inputs of forward and steer

    float forwardMovingInput = -1 + 2 * CurrentState[EFuzzyState::VE_MOVING];

    // input of rotating
    float steerInput;
    if (bIsForward(CalculateTargetAngle(GetPawn(), GetDestination(0))))
    {
        steerInput = SteerThrottleInput * CurrentState[EFuzzyState::VE_ROTATING];
    }
    else 
    {
        steerInput = -SteerThrottleInput * CurrentState[EFuzzyState::VE_ROTATING];
    }
    float velocity = GetPawn()->GetVelocity().Size();
    float res;
    if (FMath::Abs(forwardMovingInput) > FMath::Abs(steerInput))
    {
        res = forwardMovingInput;
    }
    else
    {
        res = steerInput;
    }

    if (velocity < 10 * 10000.0f / 60 / 60 && FMath::Abs(res) < 0.5f) {
        res = 0.5f*FMath::Sign(res);
    }
    return res;
}

float AFuzzyAIController::CalculateSteeringInput() const
{
    if (CurrentState[EFuzzyState::VE_END] == 1.0f)
        return 0;
    //Calculating multiplier
    float angle = CalculateTargetAngle(GetPawn(), GetDestination(0));
    // when target in front, we rotate to target, otherwise we rotate from it
    float mult = ((bIsForward(angle))^(angle<0))?
        1:
        -1;
    FVector velocity = GetPawn()->GetVelocity();
    float speedAngle = GetPawn()->GetActorRotation().Yaw - velocity.ToOrientationRotator().Yaw;
   /* if (bIsForward(angle) && FMath::Abs(speedAngle) >= 90 && velocity.Size() > 10 * 10000.0f / 60 / 60)
    {
        mult *= -1;
    }*/
    return mult * CurrentState[EFuzzyState::VE_ROTATING];
}

float AFuzzyAIController::CalculateBrakeInput() const
{
    return CurrentState[EFuzzyState::VE_STOPPING];
}

bool AFuzzyAIController::CalculateHandBrakeInput() const
{
   APawn* pawn = GetPawn();
   FVector vel = pawn->GetVelocity();
   float speedAngle = (vel.ToOrientationRotator() - (GetDestination(0) - pawn->GetActorLocation()).ToOrientationRotator()).Yaw; // Angle between speed and target
   float carSpeedAngle = vel.ToOrientationRotator().Yaw - pawn->GetActorRotation().Yaw;
   float val = AND(IsAngleBig(speedAngle), IsVelocityBig());
  // val = AND(NOT(IsAngleBig(carSpeedAngle)), val);
   val = AND(CurrentState[EFuzzyState::VE_ROTATING], val);
   return val >= 0.5f;
}

float AFuzzyAIController::IsTargetReached(const FVector & loc, const FVector & target) const
{
    return IsTargetReached(loc, target, ReachedDistanceStart);
}

float AFuzzyAIController::IsTargetReached(const FVector & loc, const FVector & target, const float StartDistance) const
{
    float distance = FMath::Sqrt(FVector::DistSquaredXY(loc, target));
    if (distance > StartDistance)
        return 0;
    if (distance <= ReachedDistanceStrict)
        return 1;
    return 1-(distance - ReachedDistanceStrict) / (StartDistance - ReachedDistanceStrict);
}

float AFuzzyAIController::IsVelocityBig() const
{
    float velocity = GetPawn()->GetVelocity().Size();
    if (velocity <= SmallVelocityLevel)
        return 0;
    if (velocity >= BigVelocityLevel)
        return 1;
    return (velocity - SmallVelocityLevel) / (BigVelocityLevel - SmallVelocityLevel);
}

float AFuzzyAIController::IsAngleBig(float angle) const
{
    if (-45 <= angle && angle <= 45)
        return 0;
    if (-100 <= angle && angle <= 100)
        return 1;
    return (FMath::Abs(angle) - 45) / 55;
}

float AFuzzyAIController::CalculateTargetAngle(const APawn* pawn, const FVector& targetLoc) const
{
    FVector currentLoc = pawn->GetActorLocation();
    FRotator rotation = pawn->GetActorRotation();
    FRotator targetRot = (targetLoc - currentLoc).ToOrientationRotator();
    return FRotator::NormalizeAxis(targetRot.Yaw - rotation.Yaw);
}

FVector AFuzzyAIController::GetDestination(int tNum) const
{
    tNum = tNum + DestinationIndex;
    if (tNum >= Route.Num())
        tNum = Route.Num() - 1;
    return Route[tNum]->GetActorLocation();
}
