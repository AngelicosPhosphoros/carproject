// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Engine/TargetPoint.h"
#include "NamedWheeledVehicle.h"
#include "MissionInfo.generated.h"

UCLASS()
class VPROJECT_API AMissionInfo : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMissionInfo();

    UPROPERTY(BlueprintReadOnly, EditAnyWhere, Category = "Game")
        TArray<ATargetPoint*> Route;

    UPROPERTY(BlueprintReadOnly, EditAnyWhere, Category = "Game")
        bool bPlayerFinished = false;

    void RegisterFinish(APawn* pawn);
    void UpdateHUD();

    UPROPERTY(BlueprintReadWrite, EditAnyWhere, Category = "Winners")
        TArray<ANamedWheeledVehicle*> Finishers;
    UPROPERTY(BlueprintReadWrite, EditAnyWhere, Category = "Winners")
        TArray<float> FinishTimes;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	
};
