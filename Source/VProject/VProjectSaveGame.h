// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SaveGame.h"
#include "MissionInfo.h"
#include "NamedWheeledVehicle.h"
#include "EFuzzyState.h"
#include "PlayerTargeting.h"
#include "FuzzyAIController.h"
#include "VProjectSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class VPROJECT_API UVProjectSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:
    void SaveMissionInfo(AMissionInfo* mission_info);
    void LoadMissionInfo(AMissionInfo* mission_info);

    void SavePlayerTargeting(APlayerTargeting* aPlayerTargeting);
    void LoadPlayerTargeting(APlayerTargeting* aPlayerTargeting);

    void SavePlayerInfo(ANamedWheeledVehicle* pawn);
    void LoadPlayerInfo(ANamedWheeledVehicle* pawn);

    void SaveAIInfo(const TArray<APawn*>& aiPawns);
    void LoadAIInfo(TArray<APawn*>& aiPawns);

    void SaveLevelInfo(const UObject* world);

    UFUNCTION(BlueprintCallable, Category = Basic)
        FString GetLevelName() { return LevelName; }
	
protected:

    UPROPERTY(VisibleAnyWhere, Category = "Mission Info")
        TArray<FName> FinishedActorNames;
    UPROPERTY(VisibleAnyWhere, Category = "Mission Info")
        TArray<float> FinishedTimes;
    UPROPERTY(VisibleAnyWhere, Category = "Mission Info")
        bool bPlayerFinished;
    UPROPERTY(VisibleAnyWhere, Category = "Player Pawn")
        FVector PlayerLocation;
    UPROPERTY(VisibleAnyWhere, Category = "Player Pawn")
        FRotator PlayerRotation;
    UPROPERTY(VisibleAnyWhere, Category = "Player Pawn")
        FVector PlayerVelocity;

    UPROPERTY(VisibleAnyWhere, Category = "AI Pawn")
        TArray<FName> AIPawnFNames;
    UPROPERTY(VisibleAnyWhere, Category = "AI Pawn")
        TArray<FVector> AIPawnLocations;
    UPROPERTY(VisibleAnyWhere, Category = "AI Pawn")
        TArray<FRotator> AIPawnRotations;
    UPROPERTY(VisibleAnyWhere, Category = "AI Pawn")
        TArray<FVector> AIPawnVelocities;
    // Only for fuzzy controllers
    // This will keep info about finished game or not
    UPROPERTY(VisibleAnyWhere, Category = "AI Pawn")
        TMap<FName, bool> AIPawnStates;
    UPROPERTY(VisibleAnyWhere, Category = "AI Pawn")
        TMap<FName, int> AIPawnDestinations;

    UPROPERTY(VisibleAnyWhere, Category = "Mission Info")
        FName PlayerTargetName;
    UPROPERTY(VisibleAnyWhere, Category = "Mission Info")
        uint32 PlayerCurrentTargetIndex;
    UPROPERTY(VisibleAnyWhere, Category = "Mission Info")
        bool bEnd;

    UPROPERTY(VisibleAnyWhere, Category = "Level info")
        FString LevelName;

};
