// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "WheeledVehicle.h"
#include "NamedWheeledVehicle.generated.h"

/**
 * 
 */
UCLASS()
class VPROJECT_API ANamedWheeledVehicle : public AWheeledVehicle
{
	GENERATED_BODY()
	
public:
    // Name for showing in players list
    UPROPERTY(EditAnyWhere, BlueprintReadWrite)
        FText PawnName;
    virtual void Tick(float delta_seconds) override;
protected:
    UPROPERTY(VisibleAnyWhere, Category = "Rolling")
    float knocked_time = 0;
    UPROPERTY(VisibleAnyWhere, Category = "Rolling")
    bool knocked = false;
};
