// Fill out your copyright notice in the Description page of Project Settings.

#include "VProject.h"
#include "Engine.h"
#include "VProjectHud.h"
#include "MissionInfo.h"


// Sets default values
AMissionInfo::AMissionInfo()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

void AMissionInfo::UpdateHUD()
{
    TArray<AActor*> hud;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), AVProjectHud::StaticClass(), hud);
    Cast<AVProjectHud>(hud[0])->ShowWinners(Finishers, FinishTimes);
}

void AMissionInfo::RegisterFinish(APawn * pawn)
{
    ANamedWheeledVehicle * p = Cast<ANamedWheeledVehicle>(pawn);
    if (nullptr == p)
        return;
    Finishers.Add(p);
    FinishTimes.Emplace(p->GetGameTimeSinceCreation());
    if (bPlayerFinished || pawn == UGameplayStatics::GetPlayerPawn(GetWorld(), 0))
    {
        bPlayerFinished = true;
        UpdateHUD();
    }
}

// Called when the game starts or when spawned
void AMissionInfo::BeginPlay()
{
	Super::BeginPlay();
}


