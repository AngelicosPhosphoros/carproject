Car Project (c) Angelicos Phosphoros 2017

Source code, assets and other project parts can be used for free for any purposes with this restrictions:
<ul>
<li>User must provide link to this repository in own license and add Angelicos Phosphoros to create derivative work</li>
<li>User cannot use parts of project in ways which are forbidden by Unreal Engine Samples license: https://www.unrealengine.com/eula</li>
</ul>

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
