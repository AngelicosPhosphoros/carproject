# README FOR CARPROJECT#

* This is simple racing game created to test fuzzy rule based systems as AI.
* Version 1.0

### How do I get set up? ###

To compile game you will need to install  
* Microsoft Visual Studio 2015    
* Unreal Engine 4.15   

After this you need to package game.  
1. Open project in Unreal Editor (VProject.uproject).  
2. File->Package Project -> Windows 64-bit  
3. Choose destination folder.  
4. Wait until game will be packaged.  

### How to play? ###

To run the game you would need to run file VProject.exe in folder WindowsNoEditor.  
After game start, you can choose *Play*, then choose level to run.   
You always in white car. Mission is to hit all highlighted targets, until you hit the last one. After this you will see scores table in left up corner of screen.  

Movement: W - throttle; S - throttle back, A - left, D - right, Space - handbrake.  

### Who do I talk to? ###

* You can contact me via email xuzin.timur@gmail.com